#include <iostream>

#define private public

#include "unitTesting.h"

void TestFat::SetUp() {
    fat = new Fat(0);
    std::cout << "testing start" << std::endl;
}

void TestFat::TearDown() {
    delete(fat);
    std::cout << "testing stop" << std::endl;
}


TEST_F(TestFat, link) {
    Fat::check();
    auto start = fat->_currentBlockNo;
    fat->get_items();
    for (int j = 0; j < 132; ++j) {
        fat->createItem("dir", 0);
    }
    auto end = fat->_currentBlockNo;
    EXPECT_EQ(start, end);

    fat->_items.clear();
    fat->get_items();
    auto size = fat->_items.size();
    EXPECT_EQ(134, size);
}


TEST_F(TestFat, writeData) {
    Fat::check();
    fat->getData("file1");
    //EXPECT_EQ("Hello,world", fat->getData("file1"));
    fat->createItem("file1", 1);
    std::string data(4099, 's');
    fat->writeData("file1", data);
    EXPECT_EQ(4099, fat->getData("file1").size());
}

TEST_F(TestFat, createItem) {
    Fat::check();
    EXPECT_EQ(0, fat->_currentBlockNo);
    fat->createItem("dir1",0);
    auto item = fat->findItem("dir");
    EXPECT_EQ(item.startBlockLow16, 1);
    /*
    auto v = fat->getItems(0);
    auto size = v.size();
    EXPECT_EQ(0, fat->_currentBlockNo);
    auto chain = fat->_freeBlockChain;
    EXPECT_EQ(2, chain.front());
    EXPECT_EQ(4, size);
     */
}

TEST_F(TestFat, getItems) {
    Fat::check();
    auto v = fat->getItems(0);
    auto size = v.size();
    EXPECT_EQ(2, size);
}



TEST_F(TestFat, getFreeBlock) {
    Fat::check();
    EXPECT_EQ(1, fat->get_free_block());
    EXPECT_EQ(2, fat->get_free_block());
    EXPECT_EQ(3, fat->get_free_block());
}



TEST_F(TestFat, findItem) {
    Fat::check();
    Item item = fat->findItem("dir1");
    EXPECT_EQ(0, item.size);
}


int main(int argc,char *argv[])
{
    testing::InitGoogleTest(&argc,argv);
    return RUN_ALL_TESTS();
}

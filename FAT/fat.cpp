#include <iostream>
#include <fstream>
#include <unistd.h>
#include <sys/time.h>
#include <sstream>
#include "sys/stat.h"
#include "fat.h"

const char *CHECK_SCRIPT = "../check.sh";
const char *FAT_PATH = "../fat.bin";

void Fat::fat_save() {
    std::fstream changeFat(FAT_PATH);
    u_int32_t seek = _freeBlockChain.front();
    //changeFat.SEEKG(seek, std::ios::beg);
    changeFat.write(reinterpret_cast<char *>(&seek), sizeof(seek));
    changeFat.close();
}

Fat::~Fat() {
    if (_freeBlockChain.size()) {
        fat_save();
    }
}

void Fat::check() {
    pid_t childPid = fork();

    if (childPid == 0) {
        execl(CHECK_SCRIPT, NULL);
    } else {
        wait(NULL);
    }
    check_fat();
    check_disk();
}

void Fat::check_fat() {
    std::ifstream check(FAT_PATH);
    if (!check.is_open()) {
        Debug("check fat : open error");
        exit(0);
    }
    u_int32_t data = 0;
    if (check.read(reinterpret_cast<char *>(&data), sizeof(data)) && data) {
        Log("fat has been init");
        return;
    }

    check.close();

    std::ofstream writeFat(FAT_PATH);

    //linkList head
    data = 1;
    writeFat.write(reinterpret_cast<char *>(&data), sizeof(data));

    //fat[0]
    data = 0xffffffff;
    writeFat.write(reinterpret_cast<char *>(&data), sizeof(data));

    //fat[1--202142]
    data = 2;
    for (int i = 0; i < 262143; i++, data++) {
        writeFat.write(reinterpret_cast<char *>(&data), sizeof(data));
    }

    //fat[202143]
    data = 0xffffffff;
    writeFat.write(reinterpret_cast<char *>(&data), sizeof(data));

    writeFat.close();
}

void Fat::check_disk() {
    generate_new_block(0,0);
}

const std::vector<Item> &Fat::getItems(const u_int32_t blockNo) {
    _currentBlockNo = blockNo;
    get_items();
    return _items;
}

void Fat::get_items() {
    //search the free block linkList which end of 0xffff
    u_int32_t nextBlock = _currentBlockNo;


    std::ifstream readFile;
    do {

        std::string block = "../disk/" + std::to_string(nextBlock);
        readFile.open(block);
        if (!readFile.is_open()) {
            exit(0);
        }

        std::ifstream readFat(FAT_PATH);
        if (!readFat.is_open()) {
            Debug("get_items : open error");
            exit(0);
        }
        readFat.SEEKG(nextBlock, std::ios::beg);
        readFat.read(reinterpret_cast<char *>(&nextBlock), sizeof(nextBlock));
        if (nextBlock == 0xffffffff) {
            Log("final block");
        } else {
            Log(nextBlock);
        }

        Item item;
        while (readFile.read((char *) (&item), sizeof(Item))) {
            if (item.trait == 0)
                Log(item.fileName << ":dir");
            else
                Log(item.fileName << ":file");
            _items.emplace_back(item);
        }
        readFile.close();
        Log("get_items : finish a block-" << nextBlock);
    } while (nextBlock != 0xffffffff);
}

Item Fat::findItem(const std::string &name) {
    get_items();
    return find_item(name);
}

Item Fat::find_item(const std::string &name) {
    std::vector<Item>::iterator get;
    char cmp[11];
    char *p = cmp;

    Log("find_item :is finding-" << name);
    get = std::find_if(_items.begin(), _items.end(),
                       [p, name](const Item &i) {
                           std::memset(p, 0, 11);
                           std::strcat(p, i.fileName);
                           std::strcat(p, i.extName);
                           return std::strcmp(p, name.c_str()) == 0;
                       });
    if (get == _items.end()) {
        Debug("find_item : can not find");
        return 0;
    }
    return *get;
}

bool Fat::createItem(const std::string &name, char trait) {
    std::string path("../disk/");

    u_int32_t newBlock = _currentBlockNo;

    if (_items.size() >= 128) {
        u_int32_t freeBlockFromChain = search_chain(_currentBlockNo);
        if (freeBlockFromChain) {
            newBlock = freeBlockFromChain;
            Log("createItem : search the free block from the linkList successfully");
        } else {
            u_int32_t old = newBlock;
            newBlock = get_free_block();
            if (newBlock == 0xffffffff) {
                Debug("createItem : new block error, the disk is full");
                return false;
            }
            link(old, newBlock);
            Log("createItem : get new block successfully");
        }
        Log("block :" << _currentBlockNo << " is full, get the free block:" << newBlock);
    }

    std::string::size_type pos = name.find('.');
    std::string extname = "";

    if (pos != std::string::npos) {
        extname = name.substr(pos + 1);
    }

    std::string filename = name.substr(0, pos);

    //get a new block for the item
    u_int32_t item_address = get_free_block();
    Item item = generate_new_item(filename, extname, trait, item_address);

    //write the fat information into the disk
    std::ofstream writeDisk((path + std::to_string(newBlock)), std::ios::app);
    if (!writeDisk.is_open()) {
        Debug("createItem : write item error");
    }
    // get a new disk for the folder
    if (!item.trait) {
        u_int32_t address = item.startBlockLow16;
        address += item.startBlockHight16 << 16;
        generate_new_block(address, _currentBlockNo);
    }
    writeDisk.write(reinterpret_cast<char *>(&item), sizeof(item));
    writeDisk.close();

    _items.emplace_back(item);
    return true;
}

void Fat::link(u_int32_t start, u_int32_t end) {
    std::fstream fat(FAT_PATH);
    if (!fat.is_open()) {
        Debug("link : can't open the fat to link");
    }

    fat.SEEKP(start, std::ios::beg);
    fat.write(reinterpret_cast<char *>(&end), sizeof(end));

    u_int32_t endFlag = 0xffffffff;

    fat.SEEKP(end, std::ios::beg);
    fat.write(reinterpret_cast<char *>(&endFlag), sizeof(endFlag));
    fat.close();
    Log("link" << start << "-" << end);
}

u_int32_t Fat::search_chain(u_int32_t start) {
    std::ifstream readFat(FAT_PATH);
    std::string path("../disk/");
    std::stringstream diskNo;
    struct stat info;
    u_int32_t next = start;
    while (1) {
        readFat.SEEKG(next, std::ios::beg);
        readFat.read(reinterpret_cast<char *>(&next), sizeof(next));
        if (next == 0xffffffff) {
            Debug("search_chain : no empty block on the linkList");
            return 0;
        }

        diskNo << next;
        std::string diskPath = path;
        diskPath += diskNo.str();
        stat(diskPath.c_str(), &info);
        diskNo.clear();
        if (4096 - info.st_size < 32)
            continue;
        else
            return next;
    }
}

bool Fat::writeData(const std::string &name, const std::string &data) {
    unsigned long size = data.size();
    struct stat info;
    Item item = find_item(name);
    if (!item) {
        Debug("write data : file name error");
        return false;
    } else if (!item.trait) {
        Debug("is a directory");
        return false;
    }

    u_int32_t address = 0;
    address = item.startBlockHight16;
    address = address << 16;
    address = item.startBlockLow16;

    std::string path("../disk/");
    std::stringstream getAddress;
    getAddress << address;
    stat((path + getAddress.str()).c_str(), &info);
    unsigned long remain = 4096 - info.st_size;
    if (remain < size) {
        unsigned long remainDataSize = size % remain;
        u_int32_t end = get_free_block();
        std::string fileName = path + getAddress.str();
        link(address, end);
        write_data(fileName, data.substr(0, remain));

        getAddress.str("");
        getAddress << end;
        fileName = path + getAddress.str();
        write_data(fileName, data.substr(remain));
    } else {
        std::string fileName = path + getAddress.str();
        write_data(fileName, data);
    }
    return true;
}

std::string Fat::getData(const std::string &fileName) {
    Item item = find_item(fileName);
    if (!item) {
        return "";
    } else if (!item.trait) {
        return "";
    }

    u_int32_t address = 0;
    address = item.startBlockHight16;
    address = address << 16;
    address = item.startBlockLow16;

    std::string path("../disk/");
    std::stringstream getAddress;
    std::string result = "";
    do {
        getAddress.str("");
        getAddress << address;
        std::ifstream readDisk(path + getAddress.str());

        if (!readDisk) {
            readDisk.close();
        } else {
            std::stringstream ss;
            ss << readDisk.rdbuf();
            readDisk.close();
            result += ss.str();
        }

        std::ifstream readFat(FAT_PATH);
        while (!readFat.is_open()) {
            Debug("getData:open fat error");
        }
        readFat.SEEKG(address, std::ios::beg);
        readFat.read(reinterpret_cast<char *>(&address), sizeof(address));
    } while (address != 0xffffffff);
    return result;
}

bool Fat::write_data(const std::string &filePath, const std::string &data) {
    std::ofstream writeDisk(filePath, std::ios::app);
    if (!writeDisk.is_open()) {
        Debug("write data: open file error");
        return false;
    }
    writeDisk << data;
    writeDisk.close();
    return true;
}


void Fat::generate_new_block(u_int32_t address, u_int32_t parentPath) {
    std::ofstream out;
    std::stringstream path;
    path << address;
    std::string diskName = "../disk/" + path.str();
    out.open(diskName, std::ios::out | std::ios::trunc);

    if (!out) {
        Debug("check_disk : open error");
        exit(0);
    }


    Item item = generate_new_item("..", "", 0, parentPath);
    out.write((const char *) (&item), sizeof(Item));

    Item item1 = generate_new_item(".", "", 0, address);
    std::strcpy(item1.fileName, ".");
    out.write((const char *) (&item1), sizeof(Item));

    out.close();
}

Item Fat::generate_new_item(std::string fileName, std::string extName,
                            char trait, u_int32_t startAddress) {
    Item item;
    std::strcpy(item.fileName, fileName.c_str());
    if (extName.size()) {
        std::strcpy(item.extName, extName.c_str());
    }
    item.trait = trait;

    struct timeval tv;
    struct timezone tz;
    struct tm *t;

    gettimeofday(&tv, &tz);
    t = localtime(&tv.tv_sec);

    item.size = 1;

    item.ms = (t->tm_sec % 10 * 100 + tv.tv_usec / 10000);

    item.time = 0;
    item.time = (item.time | (t->tm_sec / 2));
    item.time = (item.time | (t->tm_min << 5));
    item.time = (item.time | (t->tm_hour << 11));

    item.date = 0;
    item.date = (t->tm_mday);
    item.date = (item.date | (t->tm_mon << 5));
    item.date = (item.date | (t->tm_year << 9));

    item.accessDate = item.date;
    item.modifyDate = item.date;

    item.startBlockHight16 = (startAddress >> 16);
    item.startBlockLow16 = (startAddress);

    std::stringstream generateFile;
    generateFile << "touch ../disk/" << startAddress;

    std::fstream writeFat(FAT_PATH);
    if (!writeFat.is_open()) {
        Debug("generate new item : write start address error");
    }
    u_int32_t endFlag = 0xffffffff;
    writeFat.SEEKP(startAddress, std::ios::beg);
    writeFat.write(reinterpret_cast<char *>(&endFlag), sizeof(endFlag));
    writeFat.close();
    return item;

}

u_int32_t Fat::get_free_block() {
    u_int32_t get = 0;
    if (_freeBlockChain.size() > 1) {
        get = _freeBlockChain.front();
        _freeBlockChain.pop_front();
        fat_save();
        std::cerr << "get the freeblock" << get << std::endl;
    } else {
        std::ifstream readFat;
        readFat.open(FAT_PATH);
        if (!readFat.is_open()) {
            Debug("get_free_block : open error");
        }

        u_int32_t freeBlock = 0;

        //get the block for return;
        //if the freeblock = 0xffffffff, the disk is full;
        readFat.seekg(get, std::ios::beg);
        readFat.read(reinterpret_cast<char *>(&get), sizeof(get));
        freeBlock = get;

        if (freeBlock == 0xffffffff) {
            Debug("the disk is full");
            return freeBlock;
        }

        if (_freeBlockChain.size() == 1) {
            get = freeBlock = _freeBlockChain.front();
            _freeBlockChain.pop_front();
            fat_save();
        }

        //get other free block
        for (int i = 0; i < 10; i++) {
            readFat.SEEKG(get, std::ios::beg);
            readFat.read(reinterpret_cast<char *>(&get), sizeof(get));
            _freeBlockChain.push_back(get);
        }
        fat_save();
        std::cerr << "get the freeblock" << freeBlock << std::endl;
        return freeBlock;
    }
    return get;
}

u_int16_t Fat::getModifyTime() {
    struct timeval tv;
    struct timezone tz;
    struct tm *t;

    gettimeofday(&tv, &tz);
    t = localtime(&tv.tv_sec);
    u_int16_t date = 0;

    date = (t->tm_mday);
    date = (date | (t->tm_mon << 5));
    date = (date | (t->tm_year << 9));
    return date;
}

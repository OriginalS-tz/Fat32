#ifndef FAT_H
#define FAT_H

#include <string>
#include <vector>
#include <map>
#include <deque>
#include "item.h"

#define _NOLOG
#ifndef _NOLOG
    #define Log(x) std::cout<<x<<std::endl
    #define Debug(x) std::cerr<<x<<std::endl
#else
    #define Log(x) std::cout<<""
    #define Debug(x) std::cerr<<""
#endif

#define SEEKP(x, y) seekp((x+1)*sizeof(u_int32_t),y)
#define SEEKG(x, y) seekg((x+1)*sizeof(u_int32_t),y)

static
std::map<int, std::string> Errors =
        {
                {0, "IO ERROR"},
                {1, "ARGS ERROR"}
        };

class Fat {
public:
private:
    //u_int32_t _freeBlockIndex;
    std::deque<u_int32_t> _freeBlockChain;
    std::vector<Item> _items;
    std::vector<std::string> _dir;
    u_int32_t _currentBlockNo;

public:
    Fat(const u_int32_t no) : _currentBlockNo(no) {};

    static void check();

    const std::vector<Item> &getItems(u_int32_t blockNo);

    Item findItem(const std::string &name);

    //to-do
    bool createItem(const std::string &name, char trait);

    bool writeData(const std::string &name, const std::string &data);

    ~Fat();

private:
    static void check_fat();

    static void check_disk();

    static void generate_new_block(u_int32_t address, u_int32_t parentPath);

    static Item generate_new_item(std::string fileName, std::string extName,
                                  char trait, u_int32_t startAddress);

    void fat_save();

    Item find_item(const std::string &name);

    void link(u_int32_t start, u_int32_t end);

    u_int32_t search_chain(u_int32_t start);

    void get_items(); //depend on _currentBlockNo
    u_int32_t get_free_block();

    u_int16_t getModifyTime();

    std::string getData(const std::string &fileName);

    bool write_data(const std::string &filePath, const std::string &data);
};

#endif

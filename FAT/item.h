#ifndef ITEM_H
#define ITEM_H

#include <lzma.h>

struct Item {
    char fileName[8] = "";
    char extName[3] = "";
    char trait; //0:dir, 1:file
    char systemkeep;
    int size;
    u_int8_t ms; //Sec
    u_int8_t modifyDate;
    u_int16_t date; //
    u_int16_t time; //H-M
    u_int16_t accessDate;
    u_int16_t startBlockHight16;
    u_int16_t modifyTime;
    u_int16_t startBlockLow16;

    Item(int i) {
        size = i;
    }

    Item() {}

    operator bool() {
        return size;
    }
};
#endif //FAT32_ITEM_H

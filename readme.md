## FAT32 Virtual FileSystem

点击**[Project Board](https://app.gitkraken.com/glo/board/Wv7UJsmwzw4Aj5Zf)**查看开发进程


os : OS X

一个简单的Fat32虚拟文件系统

### 整体流程

![Flow](./Img/Flow.png)

### 项目结构

Fat lib(fat.dylib)

![Structure](./Img/Build.png)

`./FAT/fat.h`提供了虚拟文件系统的接口

可以通过这些接口拓展命令，将新的命令放置在`./cmd`中即可

返回格式如`./VFS/result.h`所示

基本命令有

- [x] ls
- [x] cd
- [x] touch
- [x] mkdir 
- [ ] cat
- [ ] edit

## About FAT32

请查看

[wiki](https://github.com/originals-tz/Fat32/wiki)

[readme.old](readme_old.md)



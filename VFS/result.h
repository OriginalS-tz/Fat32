#ifndef RESULT_H
#define RESULT_H

struct ResultSet {
    int currentBlockNo;
    int err = 0;
    char output[1000];

    ResultSet(){};
    ResultSet(int error) : err(error){};
};
#endif //FAT32_RESULT_H

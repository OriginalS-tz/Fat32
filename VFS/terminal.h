#ifndef TERMINAL_H
#define TERMINAL_H

#include "result.h"

class Terminal {
public:
    Terminal();

private:
    ResultSet exec(std::string, std::string);

    std::string handle(std::string &);
};


#endif //FAT32_TERMINAL_H

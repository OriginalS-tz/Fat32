#include <sstream>
#include <iostream>
#include <unistd.h>

#include "../FAT/fat.h"
#include "terminal.h"

Terminal::Terminal() {
    //check
    std::string binDIR = "../bin/";
    Fat::check();
    Fat i(0);
    std::string cmd, args;
    int currentBlock = 0;
    std::stringstream ss;
    while (1) {
        ss.str("");
        ss << currentBlock << " ";
        std::getline(std::cin, cmd);
        args = handle(cmd);
        if (access((binDIR + cmd).c_str(), F_OK) != -1) {
            ResultSet result = exec(binDIR + cmd, ss.str() + args);
            currentBlock = result.currentBlockNo;
            std::cout << result.output << std::endl;
        } else {
            std::cerr << "command not found :" << cmd << std::endl;
        }
    }
}

std::string Terminal::handle(std::string &cmd) {
    std::stringstream ss(cmd);
    std::string args;
    std::string temp;
    cmd = "";
    ss >> cmd;
    while (ss >> temp) {
        args += temp + " ";
    }
    return args;
}

ResultSet Terminal::exec(std::string name, std::string args) {
    ResultSet mySet;
    pid_t pid;
    int pip[2];
    char *pArgs[100];
    char **getArgs = pArgs;
    char const *pName = name.c_str();
    std::stringstream sStream(args);

    pipe(pip);
    pid = fork();
    std::memset(pArgs, 0, 100);

    std::string tmp;
    while (sStream >> tmp) {
        *getArgs = new char[tmp.length() + 1];
        std::strcpy(*getArgs, tmp.c_str());
        getArgs++;
    }

    if (pid == 0) {
        close(pip[0]);
        if (pip[1] != STDOUT_FILENO) {
            if (dup2(pip[1], STDOUT_FILENO) != STDOUT_FILENO)
                return NULL;
            close(pip[1]);
        }
        execv(pName, pArgs);
    } else if (pid > 0) {
        waitpid(pid, NULL, 0);
        read(pip[0], (char *) &mySet, sizeof(mySet));
        close(pip[1]);
        close(pip[0]);
    }
    return mySet;
}

#include <iostream>
#include <unistd.h>
#include "../FAT/fat.h"
#include "../VFS/result.h"

int main(int argc, char *argv[]) {
    u_int32_t currentBlock = atoi(argv[0]);
    Fat fat(currentBlock);
    ResultSet resultSet;
    resultSet.currentBlockNo = currentBlock;
    for (int i = 1; i < argc; i++) {
        std::string dirName(argv[1]);
        Item get = fat.findItem(dirName);
        if (get) {
            std::cerr << dirName << " is exist" << std::endl;
        } else {
            fat.createItem(dirName, 0);
            std::string output(dirName + " is create");
            strcat(resultSet.output, output.c_str());
        }
    }
    resultSet.err = 1;
    write(STDOUT_FILENO, (char *) &resultSet, sizeof(resultSet));
    return 0;
}


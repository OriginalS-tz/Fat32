#include <sstream>
#include <unistd.h>
#include <iostream>
#include "../VFS/result.h"
#include "../FAT/fat.h"

int main(int argc, char *argv[]) {
    std::stringstream ss;
    ResultSet resultSet;
    u_int32_t currentBlock = atoi(argv[0]);
    if (argc > 1) {
        std::string dirName(argv[1]);
        Fat fat(currentBlock);
        Item get = fat.findItem(dirName);
        if (!get) {
            std::cerr << "no such a directory" << std::endl;
        } else if (get.trait == 0) {
            u_int32_t address = get.startBlockHight16 << 16;
            address += get.startBlockLow16;
            resultSet.currentBlockNo = address;
            resultSet.err = 0;
            write(STDOUT_FILENO, (char *) &resultSet, sizeof(resultSet));
        } else {
            std::cerr << dirName << " is a file, not a directory" << std::endl;
        }
    } else {
        std::cerr << "few arguments" << std::endl;
    }
    resultSet.currentBlockNo = currentBlock;
    resultSet.err = 1;
    write(STDOUT_FILENO, (char *) &resultSet, sizeof(resultSet));
    return 0;
}
#include <iostream>
#include <sstream>
#include <map>
#include <vector>
#include <string>
#include "unistd.h"

#include "../VFS/result.h"
#include "../FAT/fat.h"

class LS {
private:
    bool allFlag;
    bool longInforFlag;
    std::string args;
    std::vector<char> argV;
    std::vector<std::string> filenameV;
    std::string::iterator get;
    std::string::iterator end;
    std::string result;
    int currentBlock;
    Fat *fat;
private:
    void token();

    void getArg();

    void getFile();

    void lookUp();

    void handle();

    void a(); //all file
    void l(); //long info
    void n(); //normal
public:
    LS(const std::string &args) : args(args) {
        allFlag = false;
        longInforFlag = false;
        allFlag = false;
        get = this->args.begin();
        end = this->args.end();
        token();

        currentBlock = std::atoi(filenameV[0].c_str());
        filenameV.erase(filenameV.begin());
        if (filenameV.size() == 0)
            allFlag = true;
        fat = new Fat(currentBlock);
        handle();
        //std::cerr << argV.size() << filenameV.size();
    }

    std::string getResult() {
        return result;
    }
};

void LS::lookUp() {
    get++;
    token();
}

void LS::token() {
    if (get == end) {
        return;
    }
    if (*get == ' ') {
        lookUp();
    } else if (*get == '-') {
        getArg();
    } else {
        getFile();
    }
}

void LS::getArg() {
    get++;
    while (get != end && *get != ' ') {
        argV.emplace_back(*get);
        switch (*get) {
            case 'a' :
                allFlag = true;
                break;
            case 'l' :
                longInforFlag = true;
                break;
            default:
                std::cerr << "no argument :" << *get << std::endl;
        }
        get++;
    }
    token();
}

void LS::getFile() {
    std::stringstream ss;
    while (get != end && *get != ' ') {
        ss << *get;
        get++;
    }
    //std::cout << "now:" << ss.str() << std::endl;
    std::string buf;
    ss >> buf;
    filenameV.emplace_back(buf);
    token();
}

void LS::handle() {
    if (allFlag)
        a();
    if (longInforFlag)
        l();
    else
        n();
}

void LS::a() {
    auto v = fat->getItems(currentBlock);
    filenameV.clear();
    for (auto i : v) {
        if (strcmp(i.extName, "")) {
            std::string fileName = i.fileName;
            std::string extName = i.extName;
            filenameV.emplace_back(fileName+"."+extName);
        } else {
            filenameV.emplace_back(i.fileName);
        }
    }
}

void LS::n() {
    std::stringstream ss;
    for (auto i : filenameV) {
        ss << i << "\t";
    }
    result = ss.str();
}

void LS::l() {
    std::stringstream ss;
    for (auto i : filenameV) {
        Item get = fat->findItem(i);
        if (get.size) {
            ss << get.size << "\t" << int(get.trait) << "\t" << i << "\n";
            std::string get = ss.str();
        }
    }
    result = ss.str();
}


int main(int argc, char *argv[]) {
    ResultSet i;
    i.err = 0;
    i.currentBlockNo = atoi(argv[0]);

    std::stringstream ss;

    for (int i = 0; i < argc; i++) {
        ss << argv[i] << " ";
    }
    LS ls(ss.str());
    strcpy(i.output, ls.getResult().c_str());
    write(STDOUT_FILENO, (char *) &i, sizeof(i));
    return 0;
}
